package ro.siit.cursIII;

public class Exercitiu2 {
    public static void main(String[] args) {
        final int x = 1000000;
        boolean isPrime = true;
        for (int i = 2; i <= x / 2; i++) {
            if (x % i == 0) {
                i++;
                System.out.println(i + "is a prime number");
            }
            isPrime = false;
        }
    }
}
