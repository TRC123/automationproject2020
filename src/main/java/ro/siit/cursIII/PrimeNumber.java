package ro.siit.cursIII;

public class PrimeNumber {
    public static void main(String[] args) {
        final int PRIME_TESTED = 65465465;

        boolean isPrime = true;
        for (int i = 2 ; i < PRIME_TESTED; i++) {
            if (PRIME_TESTED % i == 0) {
                isPrime = false;
                System.out.println("found divizor " + i);
            }
        }
        if (isPrime) {
            System.out.println("number is prime");
        }
        else {
            System.out.println("number is not prime");
        }
    }
}
